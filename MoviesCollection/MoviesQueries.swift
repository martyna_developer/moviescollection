//
//  MoviesQueries.swift
//  MoviesCollection
//
//  Created by Martyna Wiśnik on 01/07/2019.
//  Copyright © 2019 Martyna Wiśnik. All rights reserved.
//

import Foundation
import UIKit

class MoviesQueries {
    
    func getMovies(completion: @escaping (_ success:Bool, _ movies: [MovieModel], _ error_msg: Error?) -> Void) {
        guard let url = URL(string: Constants.moviesURL) else { return }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let dataResponse = data,
                error == nil else {
                    print("error")
                    return }
            do {
                let jsonResponse = try JSONSerialization.jsonObject(with: dataResponse, options: [])
                if let jsonResponse = jsonResponse as? [[String: AnyObject]] {
                    let movies = MovieMapper.mapMovie(response: jsonResponse)
                    completion(true, movies, nil)
                }
            } catch let parsingError {
                print("error", parsingError)
                completion(false, [], parsingError)
            }
            }
            .resume()
    }
    
}
