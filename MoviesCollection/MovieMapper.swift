//
//  MovieMapper.swift
//  MoviesCollection
//
//  Created by Martyna Wiśnik on 01/07/2019.
//  Copyright © 2019 Martyna Wiśnik. All rights reserved.
//

import Foundation

class MovieMapper {
    
    static func mapMovie(response: [[String: AnyObject]]) -> [MovieModel] {
        var movies = [MovieModel]()
        for movie in response {
            var title: String?
            var type: String?
            var plot: String?
            
            if let movieTitle = movie["title"] as? String {
                title = movieTitle
            }
            if let movieType = movie["type"] as? String {
                type = movieType
            }
            if let moviePlot = movie["plot"] as? String {
                plot = moviePlot
            }
            
            let movieModel = MovieModel(title: title, type: type, plot: plot)
            movies.append(movieModel)
        }
        return movies
    }
    
}
