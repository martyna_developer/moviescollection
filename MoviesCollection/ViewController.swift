//
//  ViewController.swift
//  MoviesCollection
//
//  Created by Martyna Wiśnik on 01/07/2019.
//  Copyright © 2019 Martyna Wiśnik. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var moviesList = MoviesListModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        initializeMoviesList()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 100
    }
    
    @objc func refresh(refreshControl: UIRefreshControl) {
        initializeMoviesList()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return moviesList.movies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "movieCell") as? MovieCell {
            cell.selectionStyle = .none
            cell.titleLabel.text = moviesList.movies[indexPath.row].title
            cell.typeLabel.text = moviesList.movies[indexPath.row].type
            cell.plotLabel.text = moviesList.movies[indexPath.row].plot
            
            return cell
        }
        return UITableViewCell()
    }
    
    func getMoviesList(_ completion: @escaping (_ success: Bool) -> Void) {
        APIController.sharedInstance.movies.getMovies(completion: { [weak self] (success, movie, error_msg) in
            if success {
                self?.moviesList.movies.append(contentsOf: movie)
                return completion(true)
            } else {
                return completion(false)
            }
        })
    }
    
    func initializeMoviesList() {
        moviesList.movies.removeAll()
        tableView.reloadData()
        loadingView.isHidden = false
        activityIndicator.startAnimating()
        print("===sooo long loading from free Heroku... if I could only have a paid one :( pls PWR")
        getMoviesList() { (success) in
            if success {
                DispatchQueue.main.async {
                    self.activityIndicator.stopAnimating()
                    self.loadingView.isHidden = true
                    self.tableView.reloadData()
                }
            }
        }
    }
    
}



