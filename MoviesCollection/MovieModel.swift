//
//  MovieModel.swift
//  MoviesCollection
//
//  Created by Martyna Wiśnik on 01/07/2019.
//  Copyright © 2019 Martyna Wiśnik. All rights reserved.
//

import Foundation

class MovieModel {
    
    var title: String?
    var type: String?
    var plot: String?
    
    init(title: String?, type: String?, plot: String?) {
        self.title = title
        self.type = type
        self.plot = plot
       
    }
}
