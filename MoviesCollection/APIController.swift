//
//  APIController.swift
//  MoviesCollection
//
//  Created by Martyna Wiśnik on 01/07/2019.
//  Copyright © 2019 Martyna Wiśnik. All rights reserved.
//

import Foundation

class APIController {
    static let sharedInstance = APIController()
    var movies = MoviesQueries()
}
