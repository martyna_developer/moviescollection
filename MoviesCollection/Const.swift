//
//  Const.swift
//  MoviesCollection
//
//  Created by Martyna Wiśnik on 01/07/2019.
//  Copyright © 2019 Martyna Wiśnik. All rights reserved.
//

import Foundation
import UIKit

class Constants {
    static let moviesURL = "https://friendly-moose-21246.herokuapp.com/movies"
}
